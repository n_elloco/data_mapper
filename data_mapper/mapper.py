import os
from enum import Enum

import yamale
import yaml

from .converter import XMLConverter, JSONConverter
from .exceptions import InvalidConfigFormatException


class Format(Enum):

    XML = 'xml'
    JSON = 'json'

    @classmethod
    def get_converter_cls(cls, key):
        """ Returns class of data converter by format

        :param str key:
        :rtype: BaseConverter
        """
        return {
            cls.XML.value: XMLConverter,
            cls.JSON.value: JSONConverter,
        }[key]


class BaseMapper(object):

    yaml_schema_filename = 'schema.yaml'

    def __init__(self, config_path):
        super().__init__()
        self.config_path = config_path

    def convert(self):
        """Launches converting of source data"""
        self.validate_config()
        with open(self.config_path) as fh:
            mapping = yaml.load(fh)
        converter_cls = Format.get_converter_cls(mapping['format'])
        return converter_cls(mapping).convert()

    def validate_config(self):
        """Validation of source config by YAML schema"""
        schema = yamale.make_schema(
            os.path.join(os.path.dirname(__file__), self.yaml_schema_filename))
        data = yamale.make_data(self.config_path)
        try:
            yamale.validate(schema, data)
        except ValueError as ex:
            raise InvalidConfigFormatException(ex)
        return data
