class MapperException(Exception):
    """Base exception for mapper"""
    pass


class InvalidConfigFormatException(MapperException):
    """Exception for wrong format of config"""
    pass


class InvalidDataSource(MapperException):
    """Exception for invalid data source"""
    pass
