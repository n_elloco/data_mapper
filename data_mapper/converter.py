from functools import partial
import json
import os
import re
import xml.etree.cElementTree as etree

import requests
import validators

from .exceptions import InvalidDataSource


class BaseConverter(object):
    """Base class for converters"""
    def __init__(self, mapping):
        self.mapping = mapping

    def convert(self):
        """Converts data"""
        data = self._get_raw_data(self.mapping['source'])
        return self._parse_data(data)

    def _get_raw_data(self, source):
        if validators.url(source):
            response = requests.get(source)
            if response.status_code != requests.codes.ok:
                raise InvalidDataSource(source)
            return response.text
        elif os.path.exists(source):
            with open(source) as fh:
                return fh.read()
        else:
            raise InvalidDataSource(source)

    def _parse_data(self, data):
        return data


class XMLConverter(BaseConverter):
    """Converter for XML sources"""

    def _parse_data(self, data):
        root = etree.fromstring(data, etree.XMLParser(encoding='utf-8'))
        ns = self._get_namespace(root)
        result = []
        items = root.findall('{0}{1}'.format(ns, self.mapping['item_name']))
        for item in items:
            new_item = {}
            for key, value in self.mapping['mapping'].items():
                attr = ''
                if '__' in key:
                    key, attr = key.split('__')

                elements = item.findall(self._get_find_path(key, ns))
                new_item[value] = self._get_value_from_elements(elements, attr)
            result.append(new_item)
        return result

    def _get_namespace(self, element):
        match = re.search(r'\{.*\}', element.tag)
        return match and match[0] or ''

    @staticmethod
    def _get_find_path(key, namespace):
        return '/'.join([
            '{}{}'.format(namespace, item) for item in key.split('->')
        ])

    def _get_value_from_elements(self, elements, attr):
        if not len(elements):
            return None

        if len(elements) == 1:
            return self._get_value_from_element(elements[0], attr)

        return list(map(
            partial(self._get_value_from_element, attr=attr), elements))

    @staticmethod
    def _get_value_from_element(element, attr=''):
        return element.get(attr) if attr else element.text


class JSONConverter(BaseConverter):
    """Converter for JSON sources"""

    def _parse_data(self, data):
        result = json.loads(data)
        # TODO:
        return result
