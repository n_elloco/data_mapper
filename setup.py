from setuptools import setup

setup(
    name='data-mapper',
    version='0.1',
    packages=['data_mapper'],
    url='',
    license='',
    author='Nikita Ekaterinchuk',
    author_email='',
    description='Data mapper for converting from different sources',
    long_description=open('README.md').read(),
    install_requires=open('requirements.txt').read(),
    include_package_data=True,
    test_suite='tests'
)
