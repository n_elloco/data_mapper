# Data Mapper Library

It allows to convert data from different sources to flat Python dict with
required fields. 

Supported source formats:

- XML
- JSON

## Installation

```bash
$ python setup.py install
```

## Configuration

For each source it is required to create a YAML file.

*Example:*

```yaml
format: xml
source: https://www.reddit.com/r/news/.rss
item_name: entry
mapping:
  author->name: author_name
  author->uri: author_external_id
  title: title
  updated: published
  id: external_id
  content: description
  link__href: url
  category__term: tag
```

- **format** - input format of source (xml or json)
- **source** - source location (URL or file path)
- **item_name** - name of an element which is needed to parse
- **mapping** - set of converting rules

### Converting rules

Converting rules are map, where key is a source attribute,
value is a result attribute.

Use `->` notation for getting nested elements:

*Source:*

```xml
<entry>
  <author>
    <name>Paul Smith</name>
  </author>
</entry>
```

*Rule:*

```yaml
item_name: entry
mapping:
  author->name: author_name
```

Use `__` notation for getting attributes from XML elements

*Source:*

```xml
<entry>
  <link href="http://site.com" />
</entry>
```

*Rule:*

```yaml
item_name: entry
mapping:
  link__href: url
```

## Using in application

```python
from data_mapper.mapper import BaseMapper

mapper = BaseMapper('../mappings/reddit.yaml')
data = mapper.convert()
```

## Testing

```bash
$ python setup.py test
```

