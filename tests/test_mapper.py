from unittest import TestCase, mock

from data_mapper.mapper import BaseMapper


class TestBaseMapperTestCase(TestCase):

    def setUp(self):
        super().setUp()
        self.mapper = BaseMapper('path')

    def test_init(self):
        self.assertEqual(self.mapper.config_path, 'path')

    @mock.patch('data_mapper.mapper.XMLConverter')
    @mock.patch('builtins.open')
    @mock.patch('yaml.load')
    def test_convert(self, m_yaml_load, m_open, m_xml_converter):
        self.mapper.validate_config = mock.Mock()
        mapping = {'format': 'xml'}
        m_yaml_load.return_value = mapping
        result = self.mapper.convert()

        self.mapper.validate_config.assert_called_once_with()
        m_open.assert_called_once_with('path')
        m_xml_converter.assert_called_once_with(mapping)
        m_xml_converter.return_value.convert.assert_called_once_with()
        self.assertEqual(
            result, m_xml_converter.return_value.convert.return_value)
