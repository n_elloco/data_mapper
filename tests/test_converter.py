from unittest import TestCase, mock

from data_mapper.converter import BaseConverter, XMLConverter
from data_mapper.exceptions import InvalidDataSource


class BaseConverterTestCase(TestCase):

    def setUp(self):
        super().setUp()
        self.converter = BaseConverter({
            'source': 'test_source'
        })

    def test_init(self):
        mapping = mock.Mock()
        converter = BaseConverter(mapping)
        self.assertEqual(converter.mapping, mapping)

    def test_convert(self):
        self.converter._get_raw_data = mock.Mock()
        self.converter._parse_data = mock.Mock()
        self.assertEqual(
            self.converter.convert(), self.converter._parse_data.return_value)

        self.converter._get_raw_data.assert_called_once_with('test_source')
        self.converter._parse_data.assert_called_once_with(
            self.converter._get_raw_data.return_value)

    @mock.patch('builtins.open')
    @mock.patch('os.path.exists')
    @mock.patch('requests.get')
    def test_get_raw_data(self, m_requests_get, m_exists, m_open):
        # test url
        test_url = 'http://habr.com'
        m_requests_get.return_value = mock.Mock(
            status_code=200,
            text='test_text',
        )
        self.assertEqual(
            self.converter._get_raw_data(test_url), 'test_text')
        m_requests_get.assert_called_once_with(test_url)

        m_requests_get.return_value.status_code = 500
        with self.assertRaises(InvalidDataSource):
            self.converter._get_raw_data(test_url)

        # test path
        m_exists.return_value = True
        test_path = '/var/path/file.yaml'
        result = self.converter._get_raw_data(test_path)
        m_open.assert_called_once_with(test_path)
        m_read = m_open.return_value.__enter__.return_value.read
        m_read.assert_called_once_with()
        self.assertEqual(result, m_read.return_value)

        m_exists.return_value = False
        with self.assertRaises(InvalidDataSource):
            self.converter._get_raw_data(test_path)


class XMLConverterTestCase(TestCase):

    def setUp(self):
        self.converter = XMLConverter({
            'item_name': 'entry',
            'mapping': {
                'author->name': 'author_name',
                'title': 'title',
                'link__href': 'url',
            }
        })

    def test_parse_data(self):
        data="""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <category term="news" label="r/news"/>
    <updated>2018-07-23T16:26:45+00:00</updated>
    <subtitle>/r/news is: real news articles</subtitle>
    <title>All news, US and international.</title>
    <entry>
        <author>
            <name>Author111</name>
            <uri>https://www.reddit.com/user/Ninjakick666</uri>
        </author>
        <category term="news" label="r/news"/>
        <link href="https://www.reddit.com/r/news/comments/917hkd/" />
        <title>Interesting title</title>
    </entry>
    <entry>
        <author>
            <name>Author222</name>
            <uri>https://www.reddit.com/user/kakaocomeback</uri>
        </author>
        <category term="news" label="r/news"/>
        <link href="https://www.reddit.com/r/news/comments/9158hq/" />
        <title>Yet another interesting title</title>
    </entry>
</feed>
        """
        result = self.converter._parse_data(data)
        self.assertEqual(result, [{
            'author_name': 'Author111',
            'title': 'Interesting title',
            'url': 'https://www.reddit.com/r/news/comments/917hkd/'
        }, {
            'author_name': 'Author222',
            'title': 'Yet another interesting title',
            'url': 'https://www.reddit.com/r/news/comments/9158hq/'
        }])
